package rwalerow

import akka.actor.{Actor, ActorSystem, Props}

case object PingMessage
case object PongMessage
case object StartMessage
case object StopMessage

class Ping extends Actor {

  var count = 0
  val remote = context.actorSelection("akka.tcp://PongSystem@pong-service:5150/user/PongActor")

  def incrementAndPrint = {
    count += 1
    println(s"${count} - ping")
  }

  override def receive = {
    case StartMessage =>
      incrementAndPrint
      remote ! PingMessage
    case PongMessage =>
      incrementAndPrint

      if(count > 99) {
        sender ! StopMessage
        println("ping stopped")
        context.stop(self)
        context.system.terminate()
      } else {
        sender ! PingMessage
      }
  }
}

object Run extends App {
  val system = ActorSystem("PingSystem")
  val pingActor = system.actorOf(Props[Ping], name = "PingActor")

  pingActor ! StartMessage
}
