import com.typesafe.sbt.packager.MappingsHelper.{contentOf, directory}

name := "vagrant-ping"

version := "1.0"

scalaVersion := "2.12.1"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor" % "2.4.16",
  "com.typesafe.akka" %% "akka-remote" % "2.4.16"
)

enablePlugins(JavaServerAppPackaging)

mainClass in Compile := Some("rwalerow.Run")

mappings in Universal ++= {
  // optional example illustrating how to copy additional directory
  directory("scripts") ++
    // copy configuration files to config directory
    contentOf("src/main/resources").toMap.mapValues("config/" + _)
}

scriptClasspath := Seq("../config/") ++ scriptClasspath.value

javaOptions += "-XX:MaxPermSize=1024"
